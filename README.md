 # transaction-analyser

# Tools
 - Gradle for build
 - jUnit for testing

# Steps to run [From project root folder or any IDE]
Build : [same added in setup file]
```sh
$ gradlew clean test shadowJar
```
Run: 
Stand alone jar [after running the previous command]
```sh
$ java -jar build/libs/transaction-analyser-all.jar --file=path --merchant=merchant --fromDate=fromDate --toDate=toDate
```

Import into IDE:
Should imported as gradle project in any IDE

we can run from IDE using ``` TransactionAnalyser.java ``` class file, by passing args or modifying main method.

```
TransactionQueryBuilder.getIntance("C:\\Users\\P1321380\\Desktop\\kanagaraj\\int\\hool\\transactions.csv").merchant("Kwik-E-Mart").fromDate("20/08/2018 12:00:00").toDate("20/08/2018 13:00:00");
```
# Process
- Step 1:
   Pass filters and build query
- Step 2:
   Get all transactions between dates and all reversed transactions after from date. Getting both the data in single list here to avoid readuce I/O operation for getting reversed list.
   ```
  lines.skip(1).map(transactionTransformar::apply).filter(queryProcessor::isApplicable).collect(Collectors.toList()); 
- Step 3:
   * Get reversed transactions from full list
- Step 4:
   * Get all valid transactions from full list
- Step 5:
   * Finally print the valid transactions count and average amount

# Design!
  - TransactionQueryBuilder
       * Easy to build with params without knowing the internals
  - TransactionQueryProcessor
      * To make filter independent. For any new filter or change in the filter we can implement easyly by change this class alone.
  - TransactionAnalyserService
      * Core class to process all of inputs from above classes

Improvements:
  - We can give more ways to dynamcially map the colmuns and filters
  
Alternative ways:
  - Was planing to do using "range aggregate extent algorithm", but that will take moretime to implement.
  
