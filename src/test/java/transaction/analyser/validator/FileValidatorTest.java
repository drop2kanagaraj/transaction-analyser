package transaction.analyser.validator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

public class FileValidatorTest {

	@Test
	public void givenValidFilePathThenSuccess() {
		Path resourceDirectory = Paths.get("src","test","resources","transactions.csv");
		String absolutePath = resourceDirectory.toFile().getAbsolutePath();
		assertTrue(FileValidator.validatePath(absolutePath));
	}

	@Test
	public void givenInvalidFilePathThenFail() {
		assertFalse(FileValidator.validatePath("invalid"));
	}
}
