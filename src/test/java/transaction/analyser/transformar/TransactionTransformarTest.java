package transaction.analyser.transformar;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import transaction.analyser.model.Transaction;

public class TransactionTransformarTest {

	TransactionTransformar transactionTransformar;

	@Before
	public void init() {
		transactionTransformar = new TransactionTransformar();
	}

	@Test
	public void givenValidStringThenSuccess() {
		String validRow = "WLMFRDGD, 20/08/2018 12:45:33, 59.99, Kwik-E-Mart, PAYMENT,";
		Transaction apply = transactionTransformar.apply(validRow);
		assertNotNull(apply);
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void givenEmptyStringThenException() {
		transactionTransformar.apply("");
	}
}
