package transaction.analyser;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

import org.junit.Test;

import transaction.analyser.util.DateUtil;

public class DateUtilTest {

	@Test
	public void givenValidDateThenSuccess() {
		LocalDateTime convert = DateUtil.convert("21/01/2020 12:42:00");
		assertNotNull(convert);
	}

	@Test(expected = DateTimeParseException.class)
	public void givenInvalidDateThenException() {
		DateUtil.convert("202/01/2020 12:42:00");
	}

}
