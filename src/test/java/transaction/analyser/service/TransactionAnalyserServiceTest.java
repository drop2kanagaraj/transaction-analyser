package transaction.analyser.service;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import transaction.analyser.util.TransactionQueryBuilder;

public class TransactionAnalyserServiceTest {
	
	private TransactionAnalyserService  transactionAnalyserService = new TransactionAnalyserService();

	@Test
	public void givenValidDataFileThenSuccess() {
		Path resourceDirectory = Paths.get("src","test","resources","transactions.csv");
		String absolutePath = resourceDirectory.toFile().getAbsolutePath();
		TransactionQueryBuilder queryBuilder = TransactionQueryBuilder.getIntance(absolutePath)
							.fromDate("20/08/2018 12:45:33")
							.toDate("20/08/2018 14:07:10");
		
		transactionAnalyserService.process(queryBuilder);
	}

	@Test(expected = Exception.class)
	public void givenInvalidDataFileThenFail() {
		Path resourceDirectory = Paths.get("src","test","resources","transactions-invalid.csv");
		String absolutePath = resourceDirectory.toFile().getAbsolutePath();
		TransactionQueryBuilder queryBuilder = TransactionQueryBuilder.getIntance(absolutePath);
		transactionAnalyserService.process(queryBuilder);
	}
}
