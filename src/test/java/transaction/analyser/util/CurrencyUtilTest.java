package transaction.analyser.util;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Test;

public class CurrencyUtilTest {

	@Test
	public void givenValidValueThenSuccess() {
		BigDecimal valueOf = CurrencyUtil.valueOf("22.22");
		assertEquals("22.22", valueOf.toString());
	}

	@Test(expected = NumberFormatException.class)
	public void givenValidValueThenException() {
		CurrencyUtil.valueOf("abc");
	}

	@Test
	public void givenNullValueThenSuccess() {
		BigDecimal valueOf = CurrencyUtil.valueOf(null);
		assertEquals(BigDecimal.ZERO, valueOf);
	}
}
