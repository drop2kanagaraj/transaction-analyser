package transaction.analyser.service;

import transaction.analyser.model.Transaction;
import transaction.analyser.util.TransactionQueryBuilder;

public class TransactionQueryProcessor {

	private TransactionQueryBuilder query;
	private Transaction transaction;

	public TransactionQueryProcessor(TransactionQueryBuilder query) {
		this.query = query;
	}

	public Boolean isApplicable(Transaction transaction) {
		this.transaction = transaction;
		return validateMerchant() && validateDate();
	}

	private Boolean validateMerchant() {
		return !query.getMerchant().isPresent() || transaction.getMerchant().equalsIgnoreCase(query.getMerchant().get());
	}

	private Boolean validateDate() {

		if (query.getFromDate().isPresent() && query.getToDate().isPresent()) {
			return (isAfter() && isBefore()) || (isAfter() && isReversal());
		}

		if (query.getFromDate().isPresent()) {
			return isAfter();
		}

		if (query.getToDate().isPresent()) {
			return isBefore() || isReversal();
		}

		return true;
	}

	private Boolean isAfter() {
		return query.getFromDate().isPresent() && (transaction.getDate().isAfter(query.getFromDate().get())
				|| transaction.getDate().isEqual(query.getFromDate().get()));
	}

	private Boolean isBefore() {
		return query.getToDate().isPresent() && (transaction.getDate().isBefore(query.getToDate().get())
				|| transaction.getDate().isEqual(query.getToDate().get()));
	}
	
	private Boolean isReversal() {
		return transaction.getRelatedTransaction().isPresent();
	}
}