package transaction.analyser.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import transaction.analyser.exception.TransactionAnalyserException;
import transaction.analyser.model.Transaction;
import transaction.analyser.transformar.TransactionTransformar;
import transaction.analyser.util.TransactionQueryBuilder;

public class TransactionAnalyserService {

	private TransactionTransformar transactionTransformar = new TransactionTransformar();

	public void process(TransactionQueryBuilder queryBuilder) {
		TransactionQueryProcessor queryProcessor = new TransactionQueryProcessor(queryBuilder);
		Path path = Paths.get(queryBuilder.getPath());
		try (Stream<String> lines = Files.lines(path)) {
			//To avoid multiple file reads I/O operation, this will collect all valid transactions between give condition and also the reversed transactions after the from date.
			List<Transaction> transactionsWithReversal = lines.skip(1)
												.map(transactionTransformar::apply)
												.filter(queryProcessor::isApplicable)
												.collect(Collectors.toList());
			//Filter reversed transactions from full list 
			List<Optional<String>> reversedTransactions = transactionsWithReversal.stream()
												.filter(a -> a.getRelatedTransaction().isPresent())
												.map(Transaction::getRelatedTransaction)
												.collect(Collectors.toList());
			
			//Filter all valid transactions from full list
			List<Transaction> validTransactions = transactionsWithReversal.stream()
												.filter(isReversed(reversedTransactions))
												.collect(Collectors.toList());
			
		 	long numberOfTransaction = validTransactions.size();
		 	//Get total sum of all valid transactions value  
			BigDecimal sumOfTransactionValue = validTransactions.stream().map(Transaction::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
			//average value with 2 decimals 
			BigDecimal averageTransactionValue  = sumOfTransactionValue.divide(new BigDecimal(numberOfTransaction)).setScale(2, RoundingMode.CEILING);
			System.out.println("Number of transactions =" + numberOfTransaction);
			System.out.println("Average Transaction Value =" + averageTransactionValue);
		} catch (IOException e) {
			throw new TransactionAnalyserException("Somthing went wrong.", e);
		}
	}

	private Predicate<? super Transaction> isReversed(List<Optional<String>> reversedTransactions) {
		return a -> !a.getRelatedTransaction().isPresent() && !reversedTransactions.contains(Optional.of(a.getId()));
	}

}
