package transaction.analyser;

import java.util.HashMap;
import java.util.Map;

import transaction.analyser.service.TransactionAnalyserService;
import transaction.analyser.util.TransactionQueryBuilder;
import transaction.analyser.validator.FileValidator;

public class TransactionAnalyser {

	public static void main(String[] args) {

		TransactionAnalyserService analyserService = new TransactionAnalyserService();
		TransactionQueryBuilder transactionQueryBuilder = null;
		if (args != null && args.length > 0) {
			transactionQueryBuilder = getQuery(args);
		} else {
			transactionQueryBuilder = TransactionQueryBuilder
					.getIntance("C:\\Users\\P1321380\\Desktop\\kanagaraj\\int\\hool\\transactions.csv")
					.merchant("Kwik-E-Mart").fromDate("20/08/2018 12:00:00").toDate("20/08/2018 13:00:00");
		}

		if (FileValidator.validatePath(transactionQueryBuilder.getPath())) {
			analyserService.process(transactionQueryBuilder);
		} else {
			System.err.println("Please provide valid file path");
		}
	}

	private static TransactionQueryBuilder getQuery(String[] args) {
		Map<String, String> params = new HashMap<>();
		TransactionQueryBuilder transactionQueryBuilder = null;

		for (String string : args) {
			String[] param = string.split("=");
			params.put(param[0].substring(2).toUpperCase(), param[1]);
		}

		if (params.containsKey("FILE")) {
			transactionQueryBuilder = TransactionQueryBuilder.getIntance(params.get("FILE"));

			if (params.containsKey("MERCHANT")) {
				transactionQueryBuilder.merchant(params.get("MERCHANT"));
			}

			if (params.containsKey("FROMDATE")) {
				transactionQueryBuilder.fromDate(params.get("FROMDATE"));
			}

			if (params.containsKey("TODATE")) {
				transactionQueryBuilder.toDate(params.get("TODATE"));
			}
		}
		return transactionQueryBuilder;
	}
}
