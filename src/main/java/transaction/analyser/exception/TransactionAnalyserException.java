package transaction.analyser.exception;

@SuppressWarnings("serial")
public class TransactionAnalyserException extends RuntimeException {

	public TransactionAnalyserException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public TransactionAnalyserException(String arg0) {
		super(arg0);
	}

	public TransactionAnalyserException(Throwable arg0) {
		super(arg0);
	}

}
