package transaction.analyser.util;

import java.time.LocalDateTime;
import java.util.Optional;

public class TransactionQueryBuilder {

	private String path;
	private Optional<String> merchant = Optional.empty();
	private Optional<LocalDateTime> fromDate = Optional.empty();
	private Optional<LocalDateTime> toDate = Optional.empty();

	private TransactionQueryBuilder(String path) {
		super();
		this.path = path;
	}

	public static TransactionQueryBuilder getIntance(String path) {
		return new TransactionQueryBuilder(path);
	}

	public TransactionQueryBuilder fromDate(String fromDate) {
		this.fromDate = Optional.ofNullable(DateUtil.convert(fromDate));
		return this;
	}

	public TransactionQueryBuilder toDate(String toDate) {
		this.toDate = Optional.ofNullable(DateUtil.convert(toDate));
		return this;
	}

	public TransactionQueryBuilder merchant(String merchant) {
		this.merchant = Optional.ofNullable(merchant);
		return this;
	}

	public String getPath() {
		return this.path;
	}

	public Optional<String> getMerchant() {
		return this.merchant;
	}

	public Optional<LocalDateTime> getFromDate() {
		return this.fromDate;
	}

	public Optional<LocalDateTime> getToDate() {
		return this.toDate;
	}

}
