package transaction.analyser.util;

import java.math.BigDecimal;

public class CurrencyUtil {

	private CurrencyUtil() {
	}

	public static BigDecimal valueOf(String currency) {
		if (currency == null || currency.trim().isEmpty()) {
			return BigDecimal.ZERO;
		}
		return new BigDecimal(currency.trim());
	}
}
