package transaction.analyser.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtil {

	private DateUtil() {
	}

	public static LocalDateTime convert(String date) {
		return convert(date, "dd/MM/yyyy HH:mm:ss");
	}

	public static LocalDateTime convert(String date, String format) {
		if (date == null) {
			return null;
		}

		return LocalDateTime.parse(date.trim(), DateTimeFormatter.ofPattern(format));
	}
}
