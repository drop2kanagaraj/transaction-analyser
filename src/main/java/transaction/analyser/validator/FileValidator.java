package transaction.analyser.validator;

import java.nio.file.Paths;

public class FileValidator {

	private FileValidator() {}
	
	public static boolean validatePath(String path) {
		return Paths.get(path).toFile().exists();
	}
}
