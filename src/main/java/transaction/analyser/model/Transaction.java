package transaction.analyser.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

public class Transaction {
	private String id;
	private LocalDateTime date;
	private BigDecimal amount;
	private String merchant;
	private TransactionType type;
	private Optional<String> relatedTransaction;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getMerchant() {
		return merchant;
	}

	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public Optional<String> getRelatedTransaction() {
		return relatedTransaction;
	}

	public void setRelatedTransaction(Optional<String> relatedTransaction) {
		this.relatedTransaction = relatedTransaction;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", date=" + date + ", amount=" + amount + ", merchant=" + merchant + ", type="
				+ type + ", relatedTransaction=" + relatedTransaction + "]";
	}

}
