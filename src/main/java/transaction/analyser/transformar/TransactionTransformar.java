package transaction.analyser.transformar;

import java.util.Optional;

import transaction.analyser.model.Transaction;
import transaction.analyser.model.TransactionType;
import transaction.analyser.util.CurrencyUtil;
import transaction.analyser.util.DateUtil;

public class TransactionTransformar {

	public Transaction apply(String line) {
		String[] row = line.split(",");
		// any validation required will go here
		return map(row);
	}

	private Transaction map(String[] row) {
		Transaction transaction = new Transaction();
		transaction.setId(row[0].trim());
		transaction.setDate(DateUtil.convert(row[1].trim()));
		transaction.setAmount(CurrencyUtil.valueOf(row[2]));
		transaction.setMerchant(row[3].trim());
		transaction.setType(TransactionType.valueOf(row[4].trim().toUpperCase()));
		transaction.setRelatedTransaction(getOptional(row));
		return transaction;
	}

	private Optional<String> getOptional(String[] row) {
		if (row.length <= 5 || row[5] == null || row[5].trim().isEmpty()) {
			return Optional.empty();
		}
		
		return Optional.of(row[5].trim());
	}
}
